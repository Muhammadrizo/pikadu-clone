package uz.rizo.javademopikabu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "posts")
public class Post{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String highlight;
    @Column
    private String postText;
    @Column
    private Long likes;
    @Column
    private Date date;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    private User user;



    public int compareTo(Date o) {
        int i = date.compareTo (o);
        return  i > 0 ? -1 : (i == 0 ? 0 : 1);
    }
}
