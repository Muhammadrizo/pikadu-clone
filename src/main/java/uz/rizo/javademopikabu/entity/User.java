package uz.rizo.javademopikabu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.rizo.javademopikabu.common.UserRole;
import uz.rizo.javademopikabu.common.UserStatus;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String photoURL;
    @Column
    @Enumerated
    private UserRole userRole;
    @Column
    @Enumerated
    private UserStatus userStatus;

    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.photoURL = "img.jpg";
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList (new SimpleGrantedAuthority ("ROLE_" + this.userRole.name ()));
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
