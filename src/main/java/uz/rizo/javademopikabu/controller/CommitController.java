package uz.rizo.javademopikabu.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.rizo.javademopikabu.dto.RequestCommitDto;
import uz.rizo.javademopikabu.dto.ResponseCommitDto;
import uz.rizo.javademopikabu.service.CommitService;

import java.util.List;

@RestController
@RequestMapping("/commit")
public class CommitController {
    private final CommitService commitService;


    public CommitController(CommitService commitService) {
        this.commitService = commitService;
    }

    @PostMapping("/add")
    public ResponseEntity<Boolean> addNewCommit(@RequestBody RequestCommitDto requestCommitDto){
        return ResponseEntity.ok (commitService.addCommit (requestCommitDto));
    }

    @GetMapping("/get")
    public ResponseEntity<List<ResponseCommitDto>> getCommits() {
        return ResponseEntity.ok (commitService.getCommits ());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCommit(@PathVariable("id") Long id){
        return ResponseEntity.ok (commitService.deleteCommit(id));
    }
}
