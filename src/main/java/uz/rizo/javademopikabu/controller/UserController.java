package uz.rizo.javademopikabu.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.rizo.javademopikabu.common.Message;
import uz.rizo.javademopikabu.dto.RequestUserDto;
import uz.rizo.javademopikabu.dto.ResponseUserDto;
import uz.rizo.javademopikabu.service.UserService;

import javax.persistence.PostUpdate;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/post")
    public ResponseEntity<String> postUser(@RequestBody RequestUserDto requestUserDto){
        return ResponseEntity.ok (userService.addUser(requestUserDto).getMessage ());
    }

    @GetMapping("/get")
    public ResponseEntity<List<ResponseUserDto>> getUsers(){
        return ResponseEntity.ok (userService.getUsers());
    }

    @GetMapping("/get/{userId}")
    public ResponseEntity<List<ResponseUserDto>> getUserById(@PathVariable("userId") Long userId){
        return ResponseEntity.ok (userService.getUsersById(userId));
    }

    @PostMapping("/put")
    public ResponseEntity<String> editUserInfo(@RequestBody RequestUserDto requestUserDto){
        return ResponseEntity.ok (userService.editUserInfo(requestUserDto).getMessage ());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Message> deleteUser (@PathVariable("id") Long id) {
        return ResponseEntity.ok (userService.deleteUser(id));
    }
}
