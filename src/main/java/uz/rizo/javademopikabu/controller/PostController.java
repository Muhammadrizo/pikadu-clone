package uz.rizo.javademopikabu.controller;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.rizo.javademopikabu.common.Message;
import uz.rizo.javademopikabu.dto.RequestPostDto;
import uz.rizo.javademopikabu.dto.ResponsePostDto;
import uz.rizo.javademopikabu.service.PostService;

import java.util.List;

@RestController
@RequestMapping("/post")
@AllArgsConstructor
@Log4j2
public class PostController {
    private final PostService postService;
    @PostMapping("/add")
    public ResponseEntity<Message> addNewPost(@RequestBody RequestPostDto requestPostDto){
        return ResponseEntity.ok (postService.addNewPost(requestPostDto));
    }
    @GetMapping("/getPosts")
    public ResponseEntity<List<ResponsePostDto>> getAllPosts() {
        return ResponseEntity.ok (postService.getPosts());
    }

    @GetMapping("/getPosts/{userId}")
    public ResponseEntity<List<ResponsePostDto>> getAllPostsByUserId( @PathVariable("userId") Long userId) {
        return ResponseEntity.ok (postService.getPostsByUserId(userId));
    }

    @GetMapping("/getPostsByTime")
    public ResponseEntity<List<ResponsePostDto>> getAllPostsByTime(){
        return ResponseEntity.ok (postService.getPostsByTime());
    }
 }
