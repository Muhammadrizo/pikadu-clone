package uz.rizo.javademopikabu.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    public static final String DELETED = "Удалено";
    public static final String ADD_POST = "Вы успешно добавили пост";
    public static final String POST_DID_NOT_ADD = "Пост не добавлено";
    public static final String USER_REGISTERED_SUCCSESSFULLY = "Вы регитровали успешно!!!";
    public static final String EMAIL_ALREADY_REGISTERED = "Этот Email уже занят!!!";
    public static final String REGISTRATION_ERROR = "Вы не могли регистроваться!!! Еще попробувейте";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String EDITED_INFO = "Ваша информация изменено";
    public static final String DELETE_USER = "Пользователь удалено";
    public static final String DELETED_USER_PHOTO_URL = "deleteAccount.jpg";
    private String message;
}
