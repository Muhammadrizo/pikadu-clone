package uz.rizo.javademopikabu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.rizo.javademopikabu.dto.ResponseCommitDto;
import uz.rizo.javademopikabu.entity.Commit;

import java.util.List;

@Repository
public interface CommitRepository extends JpaRepository<Commit,Long> {
    List<Commit> findAllByPostId(Long id);
}
