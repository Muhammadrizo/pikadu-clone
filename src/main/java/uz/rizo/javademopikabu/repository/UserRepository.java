package uz.rizo.javademopikabu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.rizo.javademopikabu.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findByEmail(String name);

    List<User> findAllById(Long userId);

    Optional<User> findByName(String username);
}
