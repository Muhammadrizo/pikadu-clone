package uz.rizo.javademopikabu.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.rizo.javademopikabu.common.Message;
import uz.rizo.javademopikabu.common.UserRole;
import uz.rizo.javademopikabu.common.UserStatus;
import uz.rizo.javademopikabu.dto.RequestUserDto;
import uz.rizo.javademopikabu.dto.ResponsePostDto;
import uz.rizo.javademopikabu.dto.ResponseUserDto;
import uz.rizo.javademopikabu.entity.User;
import uz.rizo.javademopikabu.repository.PostRepository;
import uz.rizo.javademopikabu.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final Logger logger = LogManager.getLogger (UserService.class);

    public UserService(UserRepository userRepository, PostRepository postRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /* Add new User*/
    public Message addUser(RequestUserDto requestUserDto) {
        Message message = new Message ();
        if (isEmpty (requestUserDto)) {
            if (userRepository.findByEmail (requestUserDto.getEmail ()) == null) {
                User user = fromRequestDto (requestUserDto);
                user.setUserStatus ((UserStatus.ACTIVE));
                user.setUserRole (UserRole.USER);
                logger.info (userRepository.save (user));
                message.setMessage (Message.USER_REGISTERED_SUCCSESSFULLY);
            } else
                message.setMessage (Message.EMAIL_ALREADY_REGISTERED);
        } else message.setMessage (Message.REGISTRATION_ERROR);
        logger.info (message.toString ());
        return message;
    }

    /* Get all Users */
    public List<ResponseUserDto> getUsers() {
        List<ResponseUserDto> userDtoList = userRepository.findAll ()
                .stream ()
                .filter (user -> user.getUserStatus () != UserStatus.DELETE)
                .map (this::toResponseDto)
                .collect (Collectors.toList ());
        logger.debug (userDtoList);
        return userDtoList;
    }

    /* Checker user for Edit */
    private User editAndSave(RequestUserDto requestUserDto) {
        User user;
        Optional<User> userOptional = userRepository.findById (requestUserDto.getId ());
        if (userOptional.isPresent ()) {
            user = userOptional.get ();
            logger.info (user.toString ());  // before edit
            user.setId (requestUserDto.getId ());
            user.setName (requestUserDto.getName ());
            if (!(requestUserDto.getPhotoUrl ().isEmpty ()))
                user.setPhotoURL (requestUserDto.getPhotoUrl ());
            logger.info (user.toString ());  // after edit
        } else {
            user = null;
        }
        return user;
    }

    /* Edit info User */
    public Message editUserInfo(RequestUserDto requestUserDto) {
        User user = editAndSave (requestUserDto);
        Message message = new Message ();
        if (user == null)
            message.setMessage (Message.USER_NOT_FOUND);
        else {
            userRepository.save (user);
            message.setMessage (Message.EDITED_INFO);
        }
        logger.error (message.toString ());
        return message;
    }

    /* Delete User */
    public Message deleteUser(Long id) {
        User user;
        Message message = new Message ();
        Optional<User> optionalUser = userRepository.findById (id);
        if (optionalUser.isPresent ()) {
            user = optionalUser.get ();
            user.setUserStatus (UserStatus.DELETE);
            user.setName (Message.DELETE_USER);
            user.setPhotoURL (Message.DELETED_USER_PHOTO_URL);
            message.setMessage (Message.DELETE_USER);
            userRepository.save (user);
        } else {
            message.setMessage (Message.USER_NOT_FOUND);
        }
        logger.info (message.toString ());
        return message;
    }

    /* User isEmpty checker  */
    private boolean isEmpty(RequestUserDto requestUserDto) {
        return requestUserDto.getEmail () != null &&
                requestUserDto.getName () != null &&
                requestUserDto.getPassword () != null;
    }


    /* from User to ResponseUserDto Converter */
    private ResponseUserDto toResponseDto(User user) {
        ResponseUserDto responseUserDto = new ResponseUserDto (user.getId (), user.getName (), user.getPhotoURL ());
        List<ResponsePostDto> responsePostDtoList = postRepository.findAll ()
                .stream ()
                .filter (post -> post.getUserId ().equals (user.getId ()))
                .map (ResponsePostDto::new)
                .collect (Collectors.toList ());
        responseUserDto.setResponsePostDtoList (responsePostDtoList);
        return responseUserDto;
    }

    /* from RequestUserDto to User Converter */

    private User fromRequestDto(RequestUserDto requestUserDto) {
        return new User (requestUserDto.getName ()
                , requestUserDto.getEmail ()
                , bCryptPasswordEncoder.encode (requestUserDto.getPassword ()));
    }

    /* from User to ResponseUserDto Converter for PostService */
    public ResponseUserDto toResponseDtoInPostService(User user) {
        return new ResponseUserDto (user.getId (), user.getName (), user.getPhotoURL ());
    }

    /* Get Users By Id */
    public List<ResponseUserDto> getUsersById(Long userId) {
        return userRepository.findAllById (userId)
                .stream ()
                .map (this::toResponseDto)
                .collect (Collectors.toList ());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByName (username);
        if (userOptional.isPresent ()){
            return userOptional.get ();
        }
        throw new UsernameNotFoundException ("User ' " + username + " ' not found");
    }
}
