package uz.rizo.javademopikabu.service;

import org.springframework.stereotype.Service;
import uz.rizo.javademopikabu.common.Message;
import uz.rizo.javademopikabu.dto.RequestCommitDto;
import uz.rizo.javademopikabu.dto.ResponseCommitDto;
import uz.rizo.javademopikabu.entity.Commit;
import uz.rizo.javademopikabu.entity.Post;
import uz.rizo.javademopikabu.repository.CommitRepository;
import uz.rizo.javademopikabu.repository.PostRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommitService {
    private final CommitRepository commitRepository;
    private final PostRepository postRepository;
    public final PostService postService;

    /* CommitService Controller */
    public CommitService(CommitRepository commitRepository, PostRepository postRepository, PostService postService) {
        this.commitRepository = commitRepository;
        this.postRepository = postRepository;
        this.postService = postService;
    }

    /* Add  Commit */
    public Boolean addCommit(RequestCommitDto requestCommitDto) {
        Commit commit = fromRequestCommitDto (requestCommitDto);
        commit.setCommitAddedDate (new Date (System.currentTimeMillis ()));
        commitRepository.save (commit);
        return true;
    }

    /* Get Commits */
    public List<ResponseCommitDto> getCommits() {
        return commitRepository.findAll ()
                .parallelStream ()
                .map (this::toResponseCommitDto)
                .collect (Collectors.toList ());
    }

    /* From RequestCommitDto to Commit Converter */
    private Commit fromRequestCommitDto(RequestCommitDto requestCommitDto) {
        return new Commit (requestCommitDto.getCommit (), requestCommitDto.getPostId ());
    }

    /* From Commit to ResponseCommitDto Converter */
    private ResponseCommitDto toResponseCommitDto(Commit commit) {
        ResponseCommitDto responseCommitDto = new ResponseCommitDto ();
        Optional<Post> post = postRepository.findById (commit.getPostId ());
        post.ifPresent (commit::setPost);
        if (post.isEmpty ()) responseCommitDto = null;
        assert responseCommitDto != null;
        {
            responseCommitDto.setId (commit.getId ());
            responseCommitDto.setCommit (commit.getCommit ());
            responseCommitDto.setCommitAddedDate (commit.getCommitAddedDate ().toLocaleString ());
            responseCommitDto.setResponsePostDto (postService.postToCommit (commit.getPost ()));
        }
        return responseCommitDto;

    }

    /* Delete Commit */
    public String deleteCommit(Long id) {
        commitRepository.deleteById (id);
        return Message.DELETED;
    }
}
