package uz.rizo.javademopikabu.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;
import uz.rizo.javademopikabu.common.Message;
import uz.rizo.javademopikabu.dto.RequestPostDto;
import uz.rizo.javademopikabu.dto.ResponseCommitDto;
import uz.rizo.javademopikabu.dto.ResponsePostDto;
import uz.rizo.javademopikabu.entity.Post;
import uz.rizo.javademopikabu.entity.User;
import uz.rizo.javademopikabu.repository.CommitRepository;
import uz.rizo.javademopikabu.repository.PostRepository;
import uz.rizo.javademopikabu.repository.UserRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
@AllArgsConstructor
public class PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final CommitRepository commitRepository;

    /* Add New Post */
    public Message addNewPost(RequestPostDto requestPostDto) {
        Post post;
        if(isEmpty (requestPostDto) && fromRequestPostDto (requestPostDto) != null) {
            post = fromRequestPostDto (requestPostDto);
            post.setLikes (0L);
            post.setDate (new Date (System.currentTimeMillis ()));
            postRepository.save (post);
            return new Message (Message.ADD_POST);
        }else {
            return new Message (Message.POST_DID_NOT_ADD);
        }
    }

    /* Get All Posts */
    public List<ResponsePostDto> getPosts() {
        return postRepository.findAll ()
                .stream ()
                .map (this::toResponsePostDto)
                .collect (Collectors.toList ());
    }

    /* From Post to ResponsePostDto Converter */
    private ResponsePostDto toResponsePostDto(Post post) {
        Optional<User> userOptional = userRepository.findById (post.getUserId ());
        User user;
        ResponsePostDto responsePostDto;
        if (userOptional.isPresent ()) {
            user = userOptional.get ();
                responsePostDto = new ResponsePostDto (post.getId (),
                userService.toResponseDtoInPostService (user),
                post.getHighlight (),
                post.getPostText (),
                post.getLikes (),
                post.getDate ());
                responsePostDto.setResponseCommitDtoList (commitRepository.findAllByPostId (post.getId ())
                        .stream ()
                        .map (commit -> new ResponseCommitDto (commit.getCommit (), commit.getCommitAddedDate ()))
                        .collect(Collectors.toList())
                );
        }else responsePostDto = null;
        return responsePostDto;
    }

    /* from RequestPostDto to Post Converter */
    private Post fromRequestPostDto(RequestPostDto requestPostDto) {
        Post post = new Post ();
        if (isEmpty (requestPostDto) && userRepository.findById (requestPostDto.getUserId ()).isPresent ()) {
            post.setHighlight (requestPostDto.getHighlight ());
            post.setPostText (requestPostDto.getPostText ());
            post.setUserId (requestPostDto.getUserId ());
        }else{
            post = null;
        }
        return post;
    }

    /* Post isEmpty checker  */
    private boolean isEmpty(RequestPostDto requestPostDto){
        return (requestPostDto.getHighlight () != null && 
                requestPostDto.getPostText () != null && 
                requestPostDto.getUserId () != null); 
    }

    /* from RequestPostDto to Post Converter for Commit */
    public ResponsePostDto postToCommit(Post post){
        Optional<User> userOptional = userRepository.findById (post.getUserId ());
        User user;
        ResponsePostDto responsePostDto;
        if (userOptional.isPresent ()) {
            user = userOptional.get ();
            responsePostDto = new ResponsePostDto (post.getId (),
                    userService.toResponseDtoInPostService (user),
                    post.getHighlight (),
                    post.getPostText (),
                    post.getLikes (),
                    post.getDate ());
        }else responsePostDto = null;
        return responsePostDto;
    }

    /* Get Post By User Id*/
    public List<ResponsePostDto> getPostsByUserId(Long userId) {
        return postRepository.findAllByUserId (userId)
                .stream ()
                .map (this::toResponsePostDto)
                .collect(Collectors.toList());
    }

    public List<ResponsePostDto> getPostsByTime() {
        return postRepository.findAll ()
                .stream ()
                .sorted ((post, t1) -> post.compareTo (t1.getDate ()))
                .map (this::toResponsePostDto)
                .collect (Collectors.toList ());
    }

    /* Get Posts By Time Order Asc*/



}
