package uz.rizo.javademopikabu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.rizo.javademopikabu.entity.Post;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ResponseCommitDto {
    private Long id;
    private String commit;
    private String commitAddedDate;
    private ResponsePostDto responsePostDto;

    public ResponseCommitDto(String commit, Date commitAddedDate) {
        this.commit = commit;
        this.commitAddedDate = commitAddedDate.toLocaleString ();
    }
}
