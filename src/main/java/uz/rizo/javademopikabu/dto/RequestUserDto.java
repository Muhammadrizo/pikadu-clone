package uz.rizo.javademopikabu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class RequestUserDto {
    private Long id;
    private String name;
    private String email;
    private String password;
    private String photoUrl;

}
