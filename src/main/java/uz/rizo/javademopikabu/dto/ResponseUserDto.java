package uz.rizo.javademopikabu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ResponseUserDto {
    private Long id;
    private String userName;
    private String photoUrl;
    private List<ResponsePostDto> responsePostDtoList;

    public ResponseUserDto(Long id, String userName, String photoUrl) {
        this.id = id;
        this.userName = userName;
        this.photoUrl = photoUrl;
    }
}
