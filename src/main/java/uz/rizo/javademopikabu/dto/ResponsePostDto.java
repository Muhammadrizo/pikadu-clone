package uz.rizo.javademopikabu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.rizo.javademopikabu.entity.Post;
import uz.rizo.javademopikabu.entity.User;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ResponsePostDto {
    private Long id;
    private ResponseUserDto responseUserDto;
    private String highlight;
    private String postText;
    private Long likes;
    private String date;
    private List<ResponseCommitDto> responseCommitDtoList;

    public ResponsePostDto(Post post) {
        this.id = post.getId ();
        this.highlight = post.getHighlight ();
        this.postText = post.getPostText ();
        this.likes = post.getLikes ();
        this.date = post.getDate ().toLocaleString ();
    }

    public ResponsePostDto(Long id, ResponseUserDto responseUserDto, String highlight, String postText, Long likes, Date date) {
        this.id = id;
        this.responseUserDto = responseUserDto;
        this.highlight = highlight;
        this.postText = postText;
        this.likes = likes;
        this.date = date.toLocaleString ();
    }
}
