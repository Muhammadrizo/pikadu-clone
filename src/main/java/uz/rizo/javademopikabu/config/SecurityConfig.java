package uz.rizo.javademopikabu.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import uz.rizo.javademopikabu.service.UserService;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserService userService;
    private final BCryptPasswordEncoder passwordEncoder;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService (userService)
                .passwordEncoder (passwordEncoder);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    http.csrf ().disable ()
            .httpBasic ()
            .and ()
            .authorizeRequests ()
            .antMatchers ("/user/post") /*kursatilgan url ga kirishi mn bulganlar*/
            .anonymous ()
            .antMatchers ("/user/**") /* /user url bilan boshlangan barcha metoflarga dastup*/
            .hasRole ("ADMIN")
            .antMatchers ("/**")/* yuqorida kursatilgan url lardan tashqari qolgan barcha url larga dostup*/
            .permitAll () /* hohlagan inson kirsa buladi bez registratsiya*/
            .and ()
    .logout ();
    }
}

//.hasAnyRole("ROLE1","ROLE2") shu metod berilgan urlga quyidagi Rolga ega usrlarni barchasiga dastup beradi